import {IsNotEmpty} from 'class-validator';

export class CreateTodoDto {
    @IsNotEmpty()
    readonly description: string;

    @IsNotEmpty()
    readonly dueDate: Date;

    @IsNotEmpty()
    readonly done: boolean;
}
