import {Injectable} from '@nestjs/common';
import {TodoEntity} from './todo.entity';
import {DeleteResult, Repository} from 'typeorm';
import {InjectRepository} from '@nestjs/typeorm';
import {CreateTodoDto} from './dto/create-todo.dto';

@Injectable()
export class TodoService {
  constructor(
      @InjectRepository(TodoEntity)
      private readonly todoRepository: Repository<TodoEntity>,
  ) {}

  async findAll(): Promise<TodoEntity[]> {
    return await this.todoRepository.find();
  }
  async create(todoData: CreateTodoDto): Promise<TodoEntity> {
    const todo = new TodoEntity();
    todo.description = todoData.description;
    todo.dueDate = todoData.dueDate;
    todo.done = todoData.done;
    const newTodo = await this.todoRepository.save(todo);
    return newTodo;
  }
  async update(id: number, todoData: CreateTodoDto): Promise<TodoEntity> {
      const toUpdate = await this.todoRepository.findOne({id});
      const updated = Object.assign(toUpdate, todoData);
      const todo = await this.todoRepository.save(updated);
      return todo;
  }

  async delete(id: number): Promise<DeleteResult> {
    return await this.todoRepository.delete({id});
  }

  async findOne(where): Promise<TodoEntity> {
    const todo = await this.todoRepository.findOne(where);
    return todo;
  }

}