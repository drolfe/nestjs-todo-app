import {Module} from '@nestjs/common';
import {TodoEntity} from './todo.entity';
import {TodoService} from './todo.service';
import {TodoController} from './todo.controller';
import {TypeOrmModule} from '@nestjs/typeorm';

@Module({
    imports: [TypeOrmModule.forFeature([TodoEntity])],
    providers: [TodoService],
    controllers: [TodoController],
})
export class TodoModule {}
