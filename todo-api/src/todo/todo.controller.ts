import {Body, Controller, Delete, Get, HttpCode, Param, Post, Put} from '@nestjs/common';
import {TodoService} from './todo.service';
import {Todo} from './todo.interface';
import {CreateTodoDto} from './dto/create-todo.dto';

@Controller('todo')
export class TodoController {
  constructor(private readonly todoService: TodoService) {}

  @Get()
  @HttpCode(200)
  async findAll(): Promise<Todo[]> {
    return this.todoService.findAll();
  }

  @Post()
  @HttpCode(200)
  async create(@Body() createTodo: CreateTodoDto) {
    return this.todoService.create(createTodo);
  }

  @Get('/:id')
  @HttpCode(200)
  findOne(@Param('id') id): Promise<Todo> {
    return this.todoService.findOne({id});
  }

  @Put('/:id')
  @HttpCode(200)
  updateTodo(@Param('id') id, @Body() createTodo: CreateTodoDto): Promise<Todo> {
    return this.todoService.update(id, createTodo);
  }

  @Delete('/:id')
  @HttpCode(200)
  async deleteTodo(@Param('id') id) {
    return this.todoService.delete(id);
  }
}
