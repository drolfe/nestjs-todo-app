import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from 'typeorm';
import {UserEntity} from '../user/user.entity';

@Entity('todo')
export class TodoEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({default: ''})
    description: string;

    @Column({type: 'timestamp', default: () => 'CURRENT_TIMESTAMP'})
    dueDate: Date;

    @Column()
    done: boolean;

    @ManyToOne(type => UserEntity, user => user.todo)
    user: UserEntity;
}
