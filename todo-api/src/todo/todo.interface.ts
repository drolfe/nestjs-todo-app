export interface Todo {
    readonly description: string;
    readonly dueDate: Date;
    readonly done: boolean;
}
