import {Response, Request} from 'express';
import {
  Body,
  Controller,
  Get,
  HttpCode, HttpException, HttpStatus,
  Post,
  Request as Req,
  Response as Res,
} from '@nestjs/common';
import {UserService} from './user.service';
import {CreateUserDto} from './dto/create-user.dto';
import {LoginUserDto} from '../todo/dto/login-user.dto';
import {UserEntity} from './user.entity';
import {UserRO} from './user.interface';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get()
  @HttpCode(200)
  async findAll(
      @Res() res: Response,
  ) {
    const users = await this.userService.findAll();
    res.status(HttpStatus.OK).json(users);
  }

  @Get('/:id')
  @HttpCode(200)
  findOne() {
    return `Find one user`;
  }

  @Post()
  @HttpCode(201)
  async create(
    @Res() res: Response,
    @Req() req: Request,
    @Body() createUser: CreateUserDto) {
    const response = await this.userService.register(createUser);
    return res.status(HttpStatus.CREATED).json(response);
  }

  @Post('/login')
  async login(@Body('user') loginUserDto: LoginUserDto ): Promise<UserRO> {
    const _user = await this.userService.findUser(loginUserDto);
    const errors = {User: ' not found'};
    if (!_user) throw new HttpException({errors}, 401);

    const token  = await this.userService.generateJWT(_user);
    const {email, username} = _user;
    const user = {email, token, username};
    return {user};
  }

}
