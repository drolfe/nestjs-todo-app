import {Injectable} from '@nestjs/common';
import {Repository} from 'typeorm';
import {UserEntity} from './user.entity';
import {InjectRepository} from '@nestjs/typeorm';
import {CreateUserDto} from './dto/create-user.dto';
import * as jwt from 'jsonwebtoken';
import { SECRET } from '../config';
import {LoginUserDto} from '../todo/dto/login-user.dto';
import * as crypto from 'crypto';
import {log} from 'util';

@Injectable()
export class UserService {
  constructor(
      @InjectRepository(UserEntity)
      private readonly userRepository: Repository<UserEntity>,
  ) {}

  async findAll(): Promise<UserEntity[]> {
    // return await this.userRepository.find();
    return Promise.resolve(this.userRepository.find());
  }

  async register(createUserDto: CreateUserDto): Promise<UserEntity> {
    const user = new UserEntity();
    user.username = createUserDto.username;
    user.email = createUserDto.email;
    user.password = createUserDto.password;
    const newUser = await this.userRepository.save(user);
    return newUser;
  }

  async findUser(loginUserDto: LoginUserDto): Promise<UserEntity> {
    const findUserOption = {
      email: loginUserDto.email,
      password: crypto.createHmac('sha256', loginUserDto.password).digest('hex'),
    };
    console.log('findUserOption: ', findUserOption);

    return await this.userRepository.findOne(findUserOption);
  }

  public generateJWT(user) {
    const today = new Date();
    const exp = new Date(today);
    exp.setDate(today.getDate() + 60);

    return jwt.sign({
      id: user.id,
      username: user.username,
      email: user.email,
      exp: exp.getTime() / 1000,
    }, SECRET);
  }

  // private buildUserRO(user: UserEntity) {
  //   const userRO = {
  //     username: user.username,
  //     token: this.generateJWT(user)
  //   };
  //
  //   return {user: userRO};
  // }
}