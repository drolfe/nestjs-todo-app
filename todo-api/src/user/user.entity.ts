import {BeforeInsert, Column, Entity, OneToMany, PrimaryGeneratedColumn} from 'typeorm';
import {TodoEntity} from '../todo/todo.entity';
import {IsEmail, IsNotEmpty} from 'class-validator';
import * as crypto from 'crypto';

@Entity('user')
export class UserEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @IsEmail()
  email: string;

  @Column()
  @IsNotEmpty()
  username: string;

  @Column()
  password: string;

  @BeforeInsert()
  hasPassword() {
    this.password = crypto.createHmac('sha256', this.password).digest('hex');
  }

  @OneToMany(type => TodoEntity, todo => todo.user)
  todo: TodoEntity[];
}