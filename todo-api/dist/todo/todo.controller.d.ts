import { TodoService } from './todo.service';
import { Todo } from './todo.interface';
import { CreateTodoDto } from './create-todo.dto';
export declare class TodoController {
    private readonly todoService;
    constructor(todoService: TodoService);
    findAll(): Promise<Todo[]>;
    create(createTodoDto: CreateTodoDto): Promise<import("./todo.entity").TodoEntity>;
    updateTodo(): Promise<string>;
    deleteTodo(): Promise<string>;
}
