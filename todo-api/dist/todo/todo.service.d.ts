import { TodoEntity } from './todo.entity';
import { Repository } from 'typeorm';
export declare class TodoService {
    private readonly todoRepository;
    constructor(todoRepository: Repository<TodoEntity>);
    findAll(): Promise<TodoEntity[]>;
    create(Todo: TodoEntity): Promise<TodoEntity>;
    update(): void;
    delete(): void;
}
