export declare class CreateTodoDto {
    readonly id: number;
    readonly description: string;
    readonly dueDate: Date;
    readonly done: boolean;
}
