export declare class TodoEntity {
    id: number;
    description: string;
    dueDate: Date;
    done: boolean;
}
