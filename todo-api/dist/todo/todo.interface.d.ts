export declare class Todo {
    readonly id: number;
    readonly description: string;
    readonly dueDate: Date;
    readonly done: boolean;
}
